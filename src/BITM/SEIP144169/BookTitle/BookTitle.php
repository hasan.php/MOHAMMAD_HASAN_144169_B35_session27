<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB
{
    public $id;
    public $book_title;
    public $author_name;

    public function __construct()
    {
        parent::__construct();
    }



    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('book_title', $postVariableData)) {
            $this->book_title = $postVariableData['book_title'];

        }
        if (array_key_exists('author_name', $postVariableData)) {
            $this->author_name = $postVariableData['author_name'];
        }

    }//end of setData method

    public function store()
    {
        $arrData=array($this->book_title,$this->author_name);
        $sql = "Insert INTO booktitle(book_title,author_name) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');


    }// end of store method

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from booktitle');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();



    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from booktitle where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();

public function update(){
    $arrData=array($this->book_title,$this->author_name);

    $sql="UPDATE booktitle SET book_title = ?, author_name = ? WHERE id=".$this->id;
   /// echo $sql;die();
     $STH=$this->DBH->prepare($sql);
    $result=$STH->execute($arrData);

    Utility::redirect('index.php');



}//end of update method



public function delete(){


    //var_dump($this->id);die();

    $sql="Delete from booktitle WHERE id=".$this->id;

    $STH=$this->DBH->prepare($sql);
    $STH->execute();

    Utility::redirect('index.php');




}
    public function trash(){


        //var_dump($this->id);die();

        $sql="Update booktitle SET is_deleted=NOW() WHERE id=".$this->id;

        $STH=$this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');




    }




}




